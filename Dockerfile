FROM python:3.7
ENV PYTHONUNBUFFERED 1

RUN apt-get -qq update && apt-get install -qq -y gdal-bin gettext \
  && rm -rf /var/lib/apt/lists/*

COPY requirements.txt /tmp/
RUN pip install -r /tmp/requirements.txt --no-cache-dir

COPY . /app/
WORKDIR /app/dnd_api

RUN ./manage.py collectstatic --noinput > /dev/null
RUN mkdir -p /app/media

RUN useradd -m user && chown -R user:user /app
USER user
