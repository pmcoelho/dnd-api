from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Hero(models.Model):
    name = models.CharField(max_length=255)
    level = models.IntegerField(default=0)

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,)
    sheet = models.FileField(upload_to="hero_sheets/", null=True, blank=True)

    created_at = models.DateTimeField(_("created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("updated_at"), auto_now=True)

    class Meta:
        verbose_name = _("hero")
        verbose_name_plural = _("heroes")

    def __str__(self):
        return self.name
