from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _


class Profile(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    is_dungeon_master = models.BooleanField()

    class Meta:
        verbose_name = _("user profile")
        verbose_name_plural = _("user profiles")
