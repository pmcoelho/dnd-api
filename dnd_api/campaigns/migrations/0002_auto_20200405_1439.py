# Generated by Django 3.0.4 on 2020-04-05 14:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("heroes", "0001_initial"),
        ("campaigns", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="campaign",
            name="heroes",
            field=models.ManyToManyField(to="heroes.Hero"),
        ),
        migrations.AddField(
            model_name="session",
            name="heroes",
            field=models.ManyToManyField(to="heroes.Hero"),
        ),
    ]
