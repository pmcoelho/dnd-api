from django.contrib import admin

from .models import Campaign, Session


class SessionInline(admin.TabularInline):
    model = Session
    extra = 1


class CampaignAdmin(admin.ModelAdmin):
    inlines = (SessionInline,)


class SessionAdmin(admin.ModelAdmin):
    pass


admin.site.register(Campaign, CampaignAdmin)
admin.site.register(Session, SessionAdmin)
