from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from heroes.models import Hero


class Campaign(models.Model):

    title = models.CharField(_("title"), max_length=255)
    description = models.TextField(_("description"))

    dm = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="campaigns_as_dm",
    )
    heroes = models.ManyToManyField(Hero)

    created_at = models.DateTimeField(_("created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("updated_at"), auto_now=True)

    class Meta:
        verbose_name = _("campaign")
        verbose_name_plural = _("campaigns")


class Session(models.Model):

    campaign = models.ForeignKey(
        Campaign, on_delete=models.CASCADE, related_name="sessions"
    )

    dm = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="sessions_as_dm",
    )
    heroes = models.ManyToManyField(Hero)

    created_at = models.DateTimeField(_("created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("updated_at"), auto_now=True)

    class SessionState(models.TextChoices):
        PREPARATION = "setup", _("setup")
        ONGOING = "ongoing", _("ongoing")
        FINISHED = "finished", _("finished")

    state = models.CharField(
        max_length=15, choices=SessionState.choices, default=SessionState.PREPARATION
    )

    class Meta:
        verbose_name = _("session")
        verbose_name_plural = _("sessions")
